Link Budget Analysis
====================

Files and scripts related to the link budget analysis. All budgets were 
calculated using v2.5.5 of the IARU calculator by Jan King you can find
[here](http://www.amsatuk.me.uk/iaru/spreadsheet.htm).

#### ``/CDR``
Link budget analysis that was conducted for the CDR

#### ``/Proposal``
Link budget analysis that was conducted for the proposal

