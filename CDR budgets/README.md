# CDR link budget analysis

Here you can find the excel sheets that were used for the link budget analysis.

* ``S_band_downlink``/``UHF_link``<br />
 &nbsp;   &nbsp;  Excel sheets for calculating the budget on the S-band and UHF 
 respectively.

* ``IARU_link_params_S-band``/``IARU_link_params_UHF``<br />
 &nbsp;   &nbsp;  The parameters used for the link budget calculation. The 
calculator itself has multiple cells that are irrelevant to our
analysis so these sheets are just an easy<br /> &nbsp;   &nbsp;
way to identify the relevant parameters. The sheets also contain sources for
the chosen parameter values. 

### Using the excel parser

There is also a tiny helper script (``utils/xlparse.py``) that can automatically
parse the values from a parameter sheet into the calculator itself. To run, 
simply call:

``python -m xlparse "path_to_param_file" "path_to_target_file"``

For example:

``python -m xlparse IARU_link_params_S-band.xls AMSAT-IARU_Link_Model_Rev2.5.5.xls``

would give you the results for our link budget calculations on the S-band 
(``Downlink Budget`` worksheet on the IARU calculator)
