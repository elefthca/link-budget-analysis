"""
A tiny helper script to write at specific cells of an excel file

To run simply call ``xlparse path_xl path_params``

path_xl: The path to your excel workbook
path_params: The path to the excel file holding the data that will be changed in your target excel. A1 cell of every
sheet must contain the number of rows in that particular sheet n. Then, n rows will be read starting from the
second one. The 'B' column must contain the data sheets, 'C' the cells and 'D' the inserted values.
A sample file could look like this:
     ----------------------------------------
    |2|           |               |   |      |
    |----------------------------------------|
    | |Data rate  |Downlink Budget|B28|100000|
    |----------------------------------------|
    | |Rain Losses|Downlink Budget|B18|0.1   |
     ----------------------------------------

Keep in mind that you can have multiple tabs as long as they respect the aforementioned format.
"""
import openpyxl as xl
import sys

if __name__ == '__main__':
    wrkbk = xl.load_workbook(filename=sys.argv[1])
    wrkbk_pars = xl.load_workbook(filename=sys.argv[2], data_only=True)

    for sheet in wrkbk_pars.worksheets:
        rows = sheet['A1'].value
        for i in range(2, rows + 2):
            wsheet = sheet['B' + str(i)].value
            cell = sheet['C' + str(i)].value
            val = sheet['D' + str(i)].value
            wrkbk[wsheet][cell].value = val
        wrkbk.save(sys.argv[1])
